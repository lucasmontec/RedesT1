#!/usr/bin/env python
import cgitb
import os

cgitb.enable()
print("Content-Type: text/html;charset=utf-8\r\n\r\n")
print("Hello World!")
print("Environment<\br>")
for param in os.environ.keys():
    print("<b>%20s</b>: %s<\br>" % (param, os.environ[param]))
