#!/usr/bin/env python
import cgitb
import cgi

cgitb.enable()

def cgiFieldStorageToDict(fieldStorage):
    """ Get a plain dictionary rather than the '.value' system used by the
   cgi module's native fieldStorage class. """
    params = {}
    for key in fieldStorage.keys():
        params[key] = fieldStorage[key].value
    return params

# Create instance of FieldStorage
form = cgiFieldStorageToDict(cgi.FieldStorage())

print("Content-Type: text/html;charset=utf-8\r\n\r\n")
print("<html>")
print("<head>")
print("<title>Post sent:</title>")
print("</head>")
print("<body>")

print("<h1>Post:</h1>")
print("<ul>")

for i in form:
    print("<li>Attr {0}: {1}</li>".format(i, form[i]))

print("</ul>")

print("</body>")
print("</html>")


