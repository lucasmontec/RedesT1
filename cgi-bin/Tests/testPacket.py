from packet import Packet

#Create packets

packetA = Packet()
packetB = Packet()

#Add info to packet A

packetA.write(1, '-aux', '127.0.0.1', '192.168.56.101')

packetBytes = packetA.to_bytes()
packetB.from_bytes(packetBytes)
print(len(packetBytes))
print("packet A:\n", vars(packetA))
print("\n")
print("packet B:\n", vars(packetB))
print("\n")
print("\n")

assert (packetA==packetB), "Serialization failed! Packets (req) are different!"

#Test a response (multiple dynamic fields)

packetA.write(1, '-aux', '127.0.0.1', '192.168.56.101', \
              "This is a very big payload indeed.\n The \
              system sent this to be checked by the user \
              at client side. But I also want to test if there\
               is life in a galaxy far far away.", True, 5)

packetBytes = packetA.to_bytes()
packetB.from_bytes(packetBytes)

assert (packetA==packetB), "Serialization failed! Packets (resp) are different!"

print("packet A:\n", vars(packetA))
print("\n")
print("packet B:\n", vars(packetB))