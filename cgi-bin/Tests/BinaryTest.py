import bitstring
# http://pythonhosted.org/bitstring/index.html

byte = bitstring.pack("uint:4, uint:4, uint:8", 2, 5, 0)

print("packed ", byte.bin)
print("packed cut ", list(map(lambda x: x.uintbe, byte.cut(8))))

version, ihl, tos = byte.readlist([4, 4, 8])
#start_code, width, height = s.readlist('hex:32, 12, 12')
#
# s = pack('uint:10, hex, int:13, 0b11', 130, '3d', -23)
# a, b, c, d = s.unpack('uint:10, hex, int:13, bin:2')
#

print("version ",version)
print("ihl ",ihl)
print("tos ",tos)


big = 300
byte2 = bitstring.pack("uint:16", big)

print("packed ", byte2)

big = byte2.unpack("uint:16")

print("unpacked ", big)