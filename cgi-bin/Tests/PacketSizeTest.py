from packet import Packet
from bitstring import BitArray

#Create packets

packetA = Packet()
packetB = Packet()

#Add info to packet A

packetA.write(1, '-aux', '127.0.0.1', '192.168.56.101')

packetBytes = packetA.to_bytes_header()
print(len(packetBytes))

#Test ip packaging
encoded = Packet.address_to_bytes('127.0.0.1')
print(encoded)
print(Packet.bytes_to_address(encoded))

#WORD 1

print("Word 1")
c = BitArray(packetBytes[0:2]) #2bytes - 16bits
c2 = BitArray(packetBytes[2:4]) #2bytes - 16bits
print(c.bin + " " + c2.bin)

#WORD 2

print("Word 2")
c = BitArray(packetBytes[4:6]) #2bytes - 16bits
c2 = BitArray(packetBytes[6:8]) #2bytes - 16bits
print(c.bin + " " + c2.bin)

#WORD 3

print("Word 3")
c = BitArray(packetBytes[8:10]) #2bytes - 16bits
c2 = BitArray(packetBytes[10:12]) #2bytes - 16bits
print(c.bin + " " + c2.bin)

#WORD 4

print("Word 4")
c = BitArray(packetBytes[12:14]) #2bytes - 16bits
c2 = BitArray(packetBytes[14:16]) #2bytes - 16bits
print(c.bin + " " + c2.bin)

#WORD 5

print("Word 5")
c = BitArray(packetBytes[16:18]) #2bytes - 16bits
c2 = BitArray(packetBytes[18:20]) #2bytes - 16bits
print(c.bin + " " + c2.bin)

print(packetBytes[12:16])
print(packetBytes[16:20])