#!/usr/bin/python3

from client import Client
from packet import Packet

client = Client()
packet = Packet()


client.connect('localhost', 9001)
packet.write(1, '', '127.0.0.1', '192.168.56.101')
client.send(packet.to_bytes())
packet = client.receive()

print(packet.payload)

client.connect('localhost', 9002)
packet.write(2, '', '127.0.0.1', '192.168.56.102')
client.send(packet.to_bytes())
packet = client.receive()

print(packet.payload)

client.connect('localhost', 9003)
packet.write(3, '', '127.0.0.1', '192.168.56.103')
client.send(packet.to_bytes())
packet = client.receive()

print(packet.payload)