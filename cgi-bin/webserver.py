#!/usr/bin/python3
import cgitb
import cgi
from client import Client
from packet import Packet

cgitb.enable()

def cgiFieldStorageToDict(fieldStorage):
    """ Get a plain dictionary rather than the '.value' system used by the
   cgi module's native fieldStorage class. """
    params = {}
    for key in fieldStorage.keys():
        params[key] = fieldStorage[key].value
    return params

# Create instance of FieldStorage
form = cgiFieldStorageToDict(cgi.FieldStorage())

def isOptionValid(option):
	return ("|" not in option) and (";" not in option) and (">" not in option)

print("Content-Type: text/html;charset=utf-8\r\n\r\n")
print("<html>")
print("<head>")
print("<title>Post sent:</title>")
print("</head>")
print("<body>")

Commands = {
        "ps": 1,
        "df": 2,
        "finger": 3,
        "uptime": 4,
    }

client = Client()
packet = Packet()
show = False;

# Check if there is any command to send to daemon 1
tags = ["maq1_ps", "maq1_df", "maq1_finger", "maq1_uptime"]

for tag in tags:
    if (tag in form.keys()):
        show = True;
if(show):
    print("<h1>Maquina 1:</h1>")
for tag in tags:
    if (tag in form.keys()):
        client.connect('localhost', 9001)
        option = ''
        if(tag.replace('_', '-') in form.keys()):
            option = form[tag.replace('_', '-')]
		if(isOptionValid(option)):
		    packet.write(Commands[form[tag]], option, '127.0.0.1', '192.168.56.101')
		    client.send(packet.to_bytes())
		    packet = client.receive()
		    print("<h2>{0}:</h2>".format(form[tag]))
		    print(packet.payload)
		else:
			print("Couldn't send command with pipe or >, < operators.")


# Check if there is any command to send to daemon 2
show = False;
tags = ["maq2_ps", "maq2_df", "maq2_finger", "maq2_uptime"]
for tag in tags:
    if (tag in form.keys()):
        show = True;
if(show):
    print("<h1>Maquina 2:</h1>")
for tag in tags:
    if (tag in form.keys()):
        client.connect('localhost', 9002)
        option = ''
        if(tag.replace('_', '-') in form.keys()):
            option = form[tag.replace('_', '-')]
		if(isOptionValid(option)):
        	packet.write(Commands[form[tag]], option, '127.0.0.1', '192.168.56.102')
        	client.send(packet.to_bytes())
        	packet = client.receive()
        	print("<h2>{0}:</h2>".format(form[tag]))
        	print(packet.payload)
		else:
			print("Couldn't send command with pipe or >, < operators.")

# Check if there is any command to send to daemon 3
show = False;
tags = ["maq3_ps", "maq3_df", "maq3_finger", "maq3_uptime"]
for tag in tags:
    if (tag in form.keys()):
        show = True;
if(show):
    print("<h1>Maquina 3:</h1>")
for tag in tags:
    if (tag in form.keys()):
        client.connect('localhost', 9003)
        option = ''
        if(tag.replace('_', '-') in form.keys()):
            option = form[tag.replace('_', '-')]
		if(isOptionValid(option)):
        	packet.write(Commands[form[tag]], option, '127.0.0.1', '192.168.56.103')
        	client.send(packet.to_bytes())
        	packet = client.receive()
        	print("<h2>{0}:</h2>".format(form[tag]))
        	print(packet.payload)
		else:
			print("Couldn't send command with pipe or >, < operators.")

print("<br>")
print("<br>")
print("<form action=\"/\" method=\"GET\">")
print("<button type=\"submit\">Voltar</button>")
print("</form>")

print("</body>")
print("</html>")


