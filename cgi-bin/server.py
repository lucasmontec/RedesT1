import socketserver
import subprocess
from packet import Packet

class EchoHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    def handle(self):
        print("Connection established!")
        packet = Packet()
        message = self.request.recv(Packet.IHL*4)
        # self.request is the TCP socket connected to the client
        # reads 5 words of 32 bits to get the header, so we can get the total packet length
        packet.header_from_bytes(message)
        # packet length in words. converting it to bytes
        payload = self.request.recv(packet.length*4 - len(message))
        # gets rest of packet
        packet.payload_from_bytes(payload)
        packet.printPacket();
        #print("Received packed from client: ", vars(packet))
        print("Executing command: ", packet.command())
        # execute command
        response = subprocess.check_output(packet.command())
        # write and send a response packet
        packet.write_response(response.decode("utf-8"))
        self.request.sendall(packet.to_bytes())


class Server:
    def listen(self, port, handler=EchoHandler):
        s = socketserver.TCPServer(('localhost', port), handler)
        s.serve_forever()