import sys
import socketserver
from packet import Packet
import subprocess

PORT = 0

class ServerHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    def handle(self):
        print("Connection established!")
        packet = Packet()
        message = self.request.recv(Packet.IHL*4)
        print("Message received: ", message)
        # self.request is the TCP socket connected to the client
        # reads 5 words of 32 bits to get the header, so we can get the total packet length
        packet.header_from_bytes(message)

        # packet length in words. converting it to bytes
        payload = self.request.recv(packet.length*4 - len(message))
        print("Payload content: ", payload)
        # gets rest of packet
        packet.payload_from_bytes(payload)
        print("Received packed from client: ", packet)
        # Print packet command
        print("Command: ", packet.command())
        output = subprocess.check_output(packet.command().split(" "))
        packet.write_response(output)
        # returns the same packet
        self.request.sendall(packet.to_bytes())

if (len(sys.argv) > 1):
    PORT = int(sys.argv[1])
    print("Server started at: "+sys.argv[1])
    
from server import Server

server = Server()
server.listen(9000, ServerHandler)