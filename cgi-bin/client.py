import socket
from packet import Packet

class Client:

    def connect(self, host, port):
        # Create a socket (SOCK_STREAM means a TCP socket)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))


    def send(self, data):
        self.socket.sendall(data)

    def receive(self):
        packet = Packet()
        message = self.socket.recv(Packet.IHL*4)
        #print("Receiving packet...")
        #print("Message length: ", len(message))
        packet.header_from_bytes(message)
        # packet length in words. converting it to bytes
        payload = self.socket.recv(packet.length*4 - len(message))
        #print("Packet length: ", packet.length)
        #print("Reading whole packet...")
        packet.payload_from_bytes(payload)
        self.socket.close()

        return packet