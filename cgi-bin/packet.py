import bitstring
from bitstring import BitStream


class Packet:
    """ Handler to create a packet with header to specification """

    packet_counter = 0

    #Packet implementation version
    Version = 2

    #Packet header size
    IHL = 5

    #Commands ps(1), df(2), finger(3), uptime(4)
    Commands = {
        1: "ps",
        2: "df",
        3: "finger",
        4: "uptime",
    }

    def __init__(self):
        #Header size
        self.ihl = Packet.IHL
        #Type of service
        self.tos = 0
        #Total packet lenght
        self.length = 0
        #Packet sequence No
        self.identification = Packet.packet_counter
        Packet.packet_counter += 1
        #Flags 000 (request), 111 (response)
        self.flags = 0
        #Time to live (dcr each read)
        self.ttl = 10
        #Protocol - supported: ps(1), df(2), finger(3), uptime(4)
        self.protocol = 1
        #Checksum
        self.checksum = 0
        #Source addr
        self.source_addr = '127.0.0.1'
        #Destination addr
        self.destination_addr = '127.0.0.1'
        #Options (variable size)
        self.options_field = ""
        #Payload (variable size)
        self.payload = ""

    def __eq__(self, other):
        if self.ihl != other.ihl : return False
        if self.tos != other.tos : return False
        if self.length != other.length : return False
        if self.identification != other.identification : return False
        if self.flags != other.flags : return False
        if self.ttl != other.ttl : return False
        if self.protocol != other.protocol : return False
        if self.checksum != other.checksum : return False
        if self.source_addr != other.source_addr : return False
        if self.destination_addr != other.destination_addr : return False
        if self.options_field != other.options_field : return False
        if self.payload != other.payload : return False
        return True

    def _cut_bytes(self, obj):
        #Creates a list from the generator of the bitstream but at
        #byte size. The uintbe means uint big endian.
        return list(map(lambda x: x.uintbe, obj.cut(8)))

    def write_response(self, payload):
        """
            Write a packet to respond.

            [payload]

            Payload is the response string field with variable size
            
            This method will switch the packet src and dest addrs.
            This method will flip response bits to 1
            This method will decrease ttl by 1
        """
        self.write(self.protocol, self.options_field, self.destination_addr, self.source_addr, str(payload), True, self.ttl-1)

    def write(self, protocol, options, src_addr, dest_addr, payload="", response=False, ttl=10):
        """
            Write a packet to send.

            [protocol, options, src_addr, dest_addr, payload="", response=false, ttl=10]

            Protocol is a int 1 to 4 meaning: 1- ps, 2- df, 3- finger and 4- uptime
            Options is a string of parameters for the command on the protocol
            Source Address is the adress string from the machine sending
            Destination Address is where to send this packet
            Payload is the response string field with variable size
            Response is a boolean that indicates this packet is a response packet
            TTL is the time to live variable
        """

        self.ihl = 5 #Fixed based on header length
        self.ttl = ttl
        self.flags = 0b111 if response else 0b000
        self.source_addr = src_addr
        self.destination_addr = dest_addr
        self.protocol = protocol
        self.options_field = options
        self.payload = payload
        #Calculate total packet length, 6 fixed words plus payload size
        self.length = 6 + len(self.payload.encode())//4
        #Calculate the checksum
        self._checksum()


    def _checksum(self):
        #To-do
        return 0

    def header_from_bytes(self, obj):
        """ 
            Deserializes bytes object to this packet header
            Returns the BitStream array to continue reading if not finished
        """

        array = BitStream(obj)

        # WORD 0:

        #Read version and ignore
        array.pos += 4 #skip 4 bits
        #Read IHL
        self.ihl = array.read("uint:4")
        #Read type of service
        array.pos += 8 #skip 8 bits
        #Read total length
        self.length = array.read("uint:16")

        # WORD 1:

        #Read ID counter, flags and fragment offset
        self.identification, self.flags = array.readlist("uint:16, uint:3")
        #Skip fragment offset
        array.pos += 13 #skip 13 bits

        # WORD 2:
        
        #Read TTL packet protocol and checksum
        self.ttl, self.protocol, self.checksum = array.readlist("uint:8, uint:8, uint:16")

        # WORD 3:

        self.source_addr = Packet.bytes_to_address(array.read("bytes:4"))

        # WORD 4:

        self.destination_addr = Packet.bytes_to_address(array.read("bytes:4"))

        return array

    def payload_from_bytes(self, obj):
        """
            Deserializes bytes object to this packet payload
            Can receive a BitStream as object to continue reading it
        """

        if(type(obj) is BitStream):
            array = obj
        else:
            array = BitStream(obj)

        # WORD 5:

        self.options_field = array.read("bytes:4").decode().strip('\0')

        #Read the rest into payload
        rest = (array.len - array.pos)//8
        self.payload = array.read("bytes:"+str(rest)).decode().strip('\0')

    def from_bytes(self, obj):
        """ Deserializes bytes object to this packet """

        array = self.header_from_bytes(obj)
        self.payload_from_bytes(array)

    def to_bytes(self):
        """ Serializes packet to bytes object """

        #Creates an array
        array = bytearray()
        #Add the version, IHL, Type Of Service, total length
        byte = bitstring.pack("uint:4, uint:4, uint:8, uint:16", \
            Packet.Version, self.ihl, 0, self.length)
        #This creates a list mapping the byte cut to a byte interpretation
        array.extend(self._cut_bytes(byte))
        #Add ID counter, flags and Fragment
        byte = bitstring.pack("uint:16, uint:3, uint:13", self.identification, self.flags, 0)
        array.extend(self._cut_bytes(byte))
        #Add TTL, Protocol and Checksum
        byte = bitstring.pack("uint:8, uint:8, uint:16", \
            self.ttl, self.protocol, self._checksum())
        array.extend(self._cut_bytes(byte))
        #Add source addr
        byte = bitstring.pack("bytes:4", Packet.address_to_bytes(self.source_addr))
        array.extend(self._cut_bytes(byte))
        #Add dest addr
        byte = bitstring.pack("bytes:4", Packet.address_to_bytes(self.destination_addr))
        array.extend(self._cut_bytes(byte))
        #Add options
        byte = bitstring.pack("bytes:4", self.pad(self.options_field.encode()))
        array.extend(self._cut_bytes(byte))
        #Add payload
        array.extend(self.payload.encode())

        #return
        return bytes(array)

    def to_bytes_header(self):
        """ Serializes header to bytes object """

        #Creates an array
        array = bytearray()
        #Add the version, IHL, Type Of Service, total length
        byte = bitstring.pack("uint:4, uint:4, uint:8, uint:16", \
            Packet.Version, self.ihl, 0, self.length)
        #This creates a list mapping the byte cut to a byte interpretation
        array.extend(self._cut_bytes(byte))
        #Add ID counter, flags and Fragment
        byte = bitstring.pack("uint:16, uint:3, uint:13", self.identification, self.flags, 0)
        array.extend(self._cut_bytes(byte))
        #Add TTL, Protocol and Checksum
        byte = bitstring.pack("uint:8, uint:8, uint:16", \
            self.ttl, self.protocol, self._checksum())
        array.extend(self._cut_bytes(byte))
        #Add source addr
        byte = bitstring.pack("bytes:4", Packet.address_to_bytes(self.source_addr))
        array.extend(self._cut_bytes(byte))
        #Add dest addr
        byte = bitstring.pack("bytes:4", Packet.address_to_bytes(self.destination_addr))
        array.extend(self._cut_bytes(byte))

        #return
        return bytes(array)
    
    def command(self):
        if(len(self.options_field)>0):
            args = [str(Packet.Commands[self.protocol]), str(self.options_field)]
        else:
            args = [str(Packet.Commands[self.protocol])]
        return args
    
    def address_to_bytes(address):
        exploded = address.split(".")
        explodedInts = map(lambda x: int(x), exploded)
        return bytes(explodedInts)

    def bytes_to_address(address):
        imploded = map(lambda x: str(int(x)), address)
        imploded = '.'.join(str(x) for x in imploded) 
        return imploded

    def pad(self, bs, sz=4):
        return bs + b"\0"*(sz-len(bs))

    def printPacket(self):
        print("Header Length: ", self.ihl)    
        print("Type Of Service: ", self.tos)
        print("Packet Length: ", self.length)
        print("Packet sequence Number: ", self.identification)
        print("Flags: ", self.flags)
        print("Time to live: ", self.ttl)
        print("Protocol: ", self.protocol)
        print("Checksum: ", self.checksum)
        print("Source addr: ", self.source_addr)
        print("Destination addr: ", self.destination_addr)
        print("Options: ", self.options_field)
        print("Payload: ", self.payload)